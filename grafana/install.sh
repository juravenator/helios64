#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t\v'
cd `dirname "${BASH_SOURCE[0]:-$0}"`

GRAFANA_VERSION=7.3.4
GRAFANA_FOLDER=grafana-${GRAFANA_VERSION}
GRAFANA_STR=${GRAFANA_FOLDER}.linux-arm64

curl -OL https://dl.grafana.com/oss/release/${GRAFANA_STR}.tar.gz
tar -xzvf ${GRAFANA_STR}.tar.gz
rm -rf ${GRAFANA_STR}.tar.gz
rm -rf /etc/grafana
mv ${GRAFANA_FOLDER} /etc/grafana
cp -r dashboards /etc/grafana
curl -Lo /etc/grafana/dashboards/node_exporter.json https://grafana.com/api/dashboards/1860/revisions/23/download
cp -rf conf default /etc/grafana

echo -n "Choose an admin password: "
read -s password
echo

sed -i -E "s|admin_grafana|$password|" /etc/grafana/conf/grafana.ini

cp grafana.service /usr/lib/systemd/system
systemctl daemon-reload
systemctl enable grafana
systemctl restart grafana