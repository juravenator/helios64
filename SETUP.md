# OS installation
I downloaded Armbian Buster (Debian 10), download links in order of specificity:  
https://wiki.kobol.io/download/  
https://redirect.armbian.com/helios64/Buster_current  
https://dl.armbian.com/helios64/archive/Armbian_21.02.1_Helios64_buster_current_5.10.12.img.xz  

https://wiki.kobol.io/helios64/install/sdcard/
```bash
xz -dk Armbian_21.02.1_Helios64_buster_current_5.10.12.img.xz
lsblk
dd if=Armbian_21.02.1_Helios64_buster_current_5.10.12.img of=/dev/sdc bs=4M conv=fsync status=progress
sync # I'm paranoid
```
Insert SD card in helios, connect power, ethernet and connect the USB-C port with your PC.
```bash
picocom -b 1500000 /dev/ttyUSB0
<follow setup>
ip addr
ssh-copy-id <ip addr>
ssh <ip addr>
```

# Basic setup
```bash
# kernel headers, firmware updates, hostname, apps
sudo armbian-config
```

# Disk auto-sleep
We set disks to sleep after 30min inactivity (-S 241)
```
vim /etc/systemd/system/disksleep@.service
systemctl daemon-reload
systemctl enable --now disksleep@sda
systemctl enable --now disksleep@sdb
systemctl enable --now disksleep@sdc
systemctl enable --now disksleep@sdd
systemctl enable --now disksleep@sde
```

# ZFS

## install
The DKMS method still fails. We build from source.  
https://wiki.kobol.io/helios64/software/zfs/build-zfs/

```bash
curl -L https://wiki.kobol.io/helios64/files/zfs/build-zfs.Dockerfile | docker build -t zfs-build-ubuntu-bionic:0.1 -
curl -OL https://wiki.kobol.io/helios64/files/zfs/install-zfs.sh
bash install-zfs.sh
```

## theory

Create a vdev & pool for all 5 disks, one being a fake sparseimage.
The missing disk already contains data, which will be copied to the pool in degraded mode before it is added to the pool.

We know our drives are 4k sector drives, so we explicitly set ashift to 12 (2^12=4096).

We use raidz2 with 5 4k disks, there is an argument to not do this:
https://forums.freebsd.org/threads/raidz2-on-5-disks-specific-settings.60295/
> This has to do with the recordsize of 128KiB that gets divided over the number of disks. Example for a 3-disk RAID-Z writing 128KiB to the pool:  
> disk1: 64KiB data (part1)  
> disk2: 64KiB data (part2)  
> disk3: 64KiB parity  
> Each disk now gets 64KiB which is an exact multiple of 4KiB. This means it is efficient and fast. Now compare this with a non-optimal configuration of 4 disks in RAID-Z:  
> disk1: 42,66KiB data (part1)  
> disk2: 42,66KiB data (part2)  
> disk3: 42,66KiB data (part3)  
> disk4: 42,66KiB parity  

However  
- volblocksize sets the largest block size that can be allocated, setting this limit to larger values decreases instances of the above example occurring.  
- compression even makes the above apply to fixed-size workloads such as databases
  > Due to compression, the physical (allocated) block sizes are not powers of two, they are odd sizes like 3.5KB or 6KB. This means that we can not rely on any exact fit of (compressed) block size to the RAID-Z group width.
- you are never worse off than with one disk less (to match the 2^n+p formula)
  > more disks in the RAID-Z group is never worse for space efficiency

This presentation outlines on why to pick volblocksize=128k (=max) (default is 8K)  
https://us-east.manta.joyent.com/Joyent_Dev/public/docs/2019-06-RAIDZ_on_small_blocks.pdf  
https://www.delphix.com/blog/delphix-engineering/zfs-raidz-stripe-width-or-how-i-learned-stop-worrying-and-love-raidz

## setup

Create the root pool
```bash
# note the fake image is way bigger (8T vs 4T real disks).
# This to prevent our fake image making our vdev size a teeny bit smaller than the real disks.
dd if=/dev/zero of=/tmp/fake.img bs=1 count=0 seek=8T
zpool create -f \
  -o ashift=12 -O acltype=posixacl -O canmount=on -O compression=lz4 -O recordsize=128k \
  -O dnodesize=auto -O normalization=formD -O relatime=off -O xattr=sa -O atime=off \
  jura raidz2 \
  /dev/disk/by-id/ata-ST4000DM004-2CV104_ZFN3AVWC \
  /dev/disk/by-id/ata-ST4000DM004-2CV104_ZFN3BKLL \
  /dev/disk/by-id/ata-ST4000DM004-2CV104_ZTT0MPWE \
  /dev/disk/by-id/ata-ST4000DM004-2CV104_ZTT0MQVY \
  /tmp/fake.img
zpool offline jura /tmp/fake.img
```

Create the encrypted volume
```bash
mkdir /naskey
mount /dev/disk/by-uuid/0310-18F7 /naskey
zfs create -o canmount=noauto -o encryption=aes-256-gcm -o keylocation=file:///naskey/naskey -o keyformat=passphrase jura/vault
# to check if everything is like you want
zfs list -o name,used,avail,refer,encryptionroot,mountpoint -S encryptionroot
zfs get all jura/vault
```

Create the backup volume
```bash
zfs create jura/backups
```

For some reason, the zfs systemd services do not respect `systemctl enable zfs-import zfs-mount`, so we do it manually.
Note that this already includes docker stuff from the next section.
```bash
systemctl unmask zfs-zed zfs-import zfs-mount docker
systemctl disable zfs-zed zfs-import zfs-mount docker
vim /etc/systemd/system/zfs.service
systemctl daemon-reload
systemctl enable --now zfs
```

## docker
https://wiki.kobol.io/helios64/software/zfs/docker-zfs/
```bash
zfs create -o mountpoint=/var/lib/docker jura/docker-root
zfs create -o mountpoint=/var/lib/docker/volumes jura/docker-volumes
chmod 700 /var/lib/docker/volumes
cat <<EOF > /etc/docker/daemon.json
{
  "storage-driver": "zfs"
}
EOF
# just in case
zfs set com.sun:auto-snapshot=false jura/docker-root
zfs set com.sun:auto-snapshot=true jura/docker-volumes
```

Also see the previous section on proper zfs mount on startup.

## monitoring

An `install.sh` script is present in the `grafana`, `hdparms-exporter`, `node_exporter`, `prometheus` folders.