#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t\v'
cd `dirname "${BASH_SOURCE[0]:-$0}"`

NODE_EXPORTER_VERSION=1.2.0

cd /tmp
curl -Lo node_exporter.tar.gz https://github.com/prometheus/node_exporter/releases/download/v${NODE_EXPORTER_VERSION}/node_exporter-${NODE_EXPORTER_VERSION}.linux-arm64.tar.gz
tar xzvf node_exporter.tar.gz
cp node_exporter-${NODE_EXPORTER_VERSION}.linux-arm64/node_exporter /bin
cd -
cp node-exporter.service /usr/lib/systemd/system
systemctl daemon-reload
systemctl enable node-exporter
systemctl restart node-exporter