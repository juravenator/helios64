# My Helios64 NAS setup

A guide on how to set it up from scratch is at [SETUP.md](SETUP.md).

## startup procedure
```bash
mount /dev/disk/by-uuid/0310-18F7 /naskey
zfs load-key jura/vault
zfs mount jura/vault
systemctl start docker
```


## disk labels

| Label on case | /dev/ | Serial number |
|-|-|-|
| 5 | sda | ZFN3AVWC |
| 4 | sdb | ZFN3BKLL |
| 3 | sdc | ZTT0MPWE |
| 2 | sdd | ZTT0MQVY |
| 1 | sde | ZFN3AW1F |