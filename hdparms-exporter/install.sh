#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t\v'
cd `dirname "${BASH_SOURCE[0]:-$0}"`

mkdir -p /etc/hdparms-exporter
cp -f main.sh /etc/hdparms-exporter
cp -f hdparms-exporter.service hdparms-exporter.timer /usr/lib/systemd/system
systemctl daemon-reload
systemctl enable hdparms-exporter.timer