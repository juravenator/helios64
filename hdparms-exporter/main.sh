#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t\v'
cd `dirname "${BASH_SOURCE[0]:-$0}"`

F_ORIG=latest.prom
F=${F_ORIG}.tmp
rm -rf $F

function hdparm-export() {
  diskpath=$1
  # hdparm_power_mode{id="ata-ST4000DM004-2CV104_ZFN3AW1F",dev="sda",status="active/idle"} 1
  id=$(basename $diskpath)
  dev=$(readlink $diskpath | rev | cut -d '/' -f1 | rev)
  status=$(hdparm -C $diskpath | grep "drive state" | rev | cut -d' ' -f1 | rev) ||:
  echo "hdparm_power_mode{id=\"${id}\",dev=\"${dev}\",status=\"${status}\"} 1" >> $F ||:
}

# for f in /dev/sd*; do
for f in /dev/disk/by-id/*; do
  # must not end with '-part[0-9]'
  if [[ ! $f =~ ^.*-part[0-9]$ ]]; then
    hdparm-export $f
  fi
done

mv -f $F $F_ORIG