#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t\v'
cd `dirname "${BASH_SOURCE[0]:-$0}"`

PROMETHEUS_VERSION=2.28.1
PROMETHEUS_STR=prometheus-${PROMETHEUS_VERSION}.linux-arm64

cd /tmp
curl -Lo ${PROMETHEUS_STR}.tar.gz https://github.com/prometheus/prometheus/releases/download/v${PROMETHEUS_VERSION}/${PROMETHEUS_STR}.tar.gz
tar xf ${PROMETHEUS_STR}.tar.gz
cp -f ${PROMETHEUS_STR}/prometheus /bin
cd -
cp -f prometheus.service /usr/lib/systemd/system
mkdir -p /etc/prometheus
cp -f conf.yaml /etc/prometheus
cp -rf rules /etc/prometheus
systemctl daemon-reload
systemctl enable prometheus
systemctl restart prometheus